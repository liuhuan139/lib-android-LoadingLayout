package com.bookbuf.library;

/**
 * Created by robert on 16/6/6.
 */
interface IStateManager {

	void bindError ();

	void bindLoading ();

//	void bindInit();

//	void bindEmpty();

//	void bindNotEmpty ();
}