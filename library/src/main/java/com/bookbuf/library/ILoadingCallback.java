package com.bookbuf.library;

/**
 * Created by robert on 16/6/6.
 */
public interface ILoadingCallback {

	/** 初始化 */
	void onInit ();

	/** 数据为空 */
	void onEmpty ();

	/** 数据不为空 */
	void onContent ();

	/** 加载失败 */
	void onError ();

	/** 加载中 */
	void onLoading ();
}