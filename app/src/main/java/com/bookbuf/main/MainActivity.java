package com.bookbuf.main;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.bookbuf.library.LoadingLayout;
import com.bookbuf.library.LoadingLayoutAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

	LoadingLayout loadingLayout;
	SimpleAdapter adapter;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_main);


		/*bind view*/
		loadingLayout = (LoadingLayout) findViewById (R.id.loadingLayout);
		adapter = new SimpleAdapter ();
		loadingLayout.setAdapter (adapter);
		loadingLayout.setRetryListener (new View.OnClickListener () {
			@Override
			public void onClick (View v) {
				adapter.bindLoading ();
				SimpleAsyncTask task = new SimpleAsyncTask ();
				task.setOnPostExecute (new OnPostExecute () {
					@Override
					public void onPostExecute (List<String> strings) {
						/*bind data*/
						adapter.bind (strings);
					}
				});
				task.execute ();
			}
		});

	}

	public void onEmptyClick (View view) {
		adapter.bindLoading ();
		SimpleAsyncTask task = new SimpleAsyncTask ();
		task.setOnPostExecute (new OnPostExecute () {
			@Override
			public void onPostExecute (List<String> strings) {
				/*bind data*/
				adapter.bind (null);
			}
		});
		task.execute ();
	}

	public void onContentClick (View view) {
		adapter.bindLoading ();
		SimpleAsyncTask task = new SimpleAsyncTask ();
		task.setOnPostExecute (new OnPostExecute () {
			@Override
			public void onPostExecute (List<String> strings) {
				/*bind data*/
				adapter.bind (strings);
			}
		});
		task.execute ();
	}

	public void onErrorClick (View view) {
		adapter.bindLoading ();
		SimpleAsyncTask task = new SimpleAsyncTask ();
		task.setOnPostExecute (new OnPostExecute () {
			@Override
			public void onPostExecute (List<String> strings) {
				/*bind data*/
				adapter.bindError ();
			}
		});
		task.execute ();
	}

	private class SimpleAdapter extends LoadingLayoutAdapter<List<String>> {

		@Override
		public boolean isEmpty () {
			return data == null || data.isEmpty ();
		}
	}

	private class SimpleAsyncTask extends AsyncTask<String, Integer, List<String>> {

		private OnPostExecute onPostExecute;

		public void setOnPostExecute (OnPostExecute onPostExecute) {
			this.onPostExecute = onPostExecute;
		}

		@Override
		protected List<String> doInBackground (String... params) {


			try {
				Thread.sleep (2000);
			} catch (InterruptedException e) {
				e.printStackTrace ();
			}

			List<String> strings = new ArrayList<> ();
			strings.add (" data 1");
			strings.add (" data 2");
			return strings;
		}

		@Override
		protected void onPostExecute (List<String> strings) {
			super.onPostExecute (strings);
			if (onPostExecute != null) {
				onPostExecute.onPostExecute (strings);
			}
		}
	}

	private interface OnPostExecute {
		void onPostExecute (List<String> strings);
	}
}
